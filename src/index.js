var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var watch = require('node-watch');
var r = /^.*\.(xml|lua)$/;

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});

var filter = function(pattern, fn) {
  return function(filename) {
    if (pattern.test(filename)) {
      fn(filename);
    }
  }
}
 
watch('../', { recursive: true, followSymLinks: true }, filter(/^.*\.(xml|lua)$/, function(filename) {
  io.emit('file changed', { file: filename });
}));