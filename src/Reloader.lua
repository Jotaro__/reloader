----
--  Reloader 
--  @author Jotaro
--

require "lib/lib_Slash";

require "lib/lib_Debug";
Debug.EnableLogging(true)

--~~~~~~~~~~~~~--
-- Constants   --
--~~~~~~~~~~~~~--

local WEBFRAME = Component.GetFrame("Web");

--~~~~~~~~~~~~~--
-- Events      --
--~~~~~~~~~~~~~--

-- use to reload the page if needed
function OnComponentLoad()
  LIB_SLASH.BindCallback({slash_list='rl', description='', func=executeSlashCommand});
  load();
end

--~~~~~~~~~~~~~--
-- Callbacks   --
--~~~~~~~~~~~~~--

function reloadUI(args)
  Debug.Log(args.file.." has changed, reloading");
  System.ReloadUI();
end

--~~~~~~~~~~~~~--
-- Functions   --
--~~~~~~~~~~~~~--

function load()
  
    WEBFRAME:SetUrlFilters("*");
    WEBFRAME:LoadUrl("http://localhost:3000/");
    WEBFRAME:AddWebCallback("callback", reloadUI);
  
end

function executeSlashCommand(args)
    load();
end

















