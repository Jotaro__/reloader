var path = require('path');
var gulp = require('gulp');
var file = require('gulp-file');
var zip = require('gulp-zip');
var del = require('del');


var p = require('./package.json')

var root = './src'; // without trailing slash

gulp.task('files', function(){
  var files = gulp.src([
    root + '/**/*.lua',
    root + '/**/*.xml'
  ])
  return files.pipe(gulp.dest('./build/' + p.name))
});

gulp.task('melder', function() {
  var br = "\n";
  
  var str = "title=" + p.name + br +
            "author=" + p.author + br +
            "version=" + p.version + br +
            "patch=" + p.patch + br +
            "url=" + p.url + br +
            "destination=" + p.destination + br +
            "description=" + p.description;
    
  return file('melder_info.ini', str, { src: true }).pipe(gulp.dest('./build'));
});

gulp.task('zip', ['melder', 'files'], function () {
    return gulp.src('build/**/*.*')
        .pipe(zip(p.name + '_' + p.version + '.zip'))
        .pipe(gulp.dest('dist'));
});

gulp.task('clean:build', ['zip'], function (cb) {
  return del([
    './build/**'
  ], cb);
});

gulp.task('release', ['build'] , function(){
  var rel = gulp.src('dist/*.zip')
  return rel.pipe(gulp.dest('D:/Dokumente/FirefallAddons/_releases'))
});

//custom

gulp.task('package', function() {
  var pack = {
    name: p.name,
    author: p.author,
    version: p.version,
    dependencies: p.releaseDependencies,
    main: p.releaseMain
  }
    
  return file('package.json', JSON.stringify(pack, null, 2), { src: true }).pipe(gulp.dest('./src'));
});

gulp.task('app', function(){
  var files = gulp.src([
    root + '/**/*.html',
    root + '/**/*.js',
    root + '/**/*.json'
  ])
  return files.pipe(gulp.dest('./build/' + p.name))
});

// defining all task dependencies

gulp.task('copy', ['melder', 'files', 'app'])
gulp.task('build', ['copy', 'zip', 'clean:build'])
gulp.task('default', ['package', 'build', 'release'])



